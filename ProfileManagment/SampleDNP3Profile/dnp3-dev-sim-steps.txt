opendnp3 Device(client) simulator:

Building opendnp3 from code:
To build opendnp3 clone the opendnp3 repository to a directory of your choice. The branch name is release-2.x

$ git clone --recursive -b release-2.x https://github.com/dnp3/opendnp3.git
$ cd opendnp3
$ export OPENDNP3_LIB_DIR=`pwd`
$ mkdir build
$ cd build
$ cmake -DSTATICLIBS=ON -DCMAKE_POSITION_INDEPENDENT_CODE=ON -DDNP3_DEMO=ON ..
$ make

cd build
sudo ./outstation-demo


Test with Fledge###
now provide input:
This demo application listens on any IP address, port 20001 and has link Id set to 10. 
It also assumes master link Id is 1.
Same config can be set in fledge dnp3 plugin.

Once started it logs traffic and wait for use input for sending unsolicited messages:

Enter one or more measurement changes then press <enter>
c = counter, b = binary, d = doublebit, a = analog, o = octet string, 'quit' = exit

for test fledge: enter c, and it will send one reading to fledge like below:
asset: dnp3_01remote_10_counter_0; Timestamp:**** ; readings: {"count0": 1}


reference steps:
https://github.com/fledge-iot/fledge-south-dnp3


